package com.myapps.bookextractor.safari;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.htmlparser.Parser;
import org.htmlparser.Tag;
import org.htmlparser.filters.AndFilter;
import org.htmlparser.filters.HasAttributeFilter;
import org.htmlparser.filters.TagNameFilter;
import org.htmlparser.tags.Div;
import org.htmlparser.tags.LinkTag;
import org.htmlparser.tags.ScriptTag;
import org.htmlparser.util.NodeList;
import org.htmlparser.util.ParserException;

import com.myapps.bookextractor.chapter.AbstractChapterDownloader;
import com.myapps.bookextractor.downloader.HttpCookiesGetDownloader;
import com.myapps.bookextractor.downloader.HttpDownloader;
import com.myapps.bookextractor.toc.TocEntry;
import com.myapps.bookextractor.utils.HTMLUtils;

public class SafBChapterDownloader extends AbstractChapterDownloader {
	
	private String bookId = null;
	
	@Override
	public void downloadTocEntries(String bookUrl, List<TocEntry> tocEntries,
			OutputStream output, String destinationFolder)
	{
		getBookIdFromUrl(bookUrl);
		
		super.downloadTocEntries(bookUrl, tocEntries, output, destinationFolder);
	}

	/**
	 * Gets the book ID fromt he book URL
	 * @param bookUrl Book URL
	 */
	private void getBookIdFromUrl(String bookUrl) {
		String[] splittedBookUrl = bookUrl.split("/");
		bookId = splittedBookUrl[splittedBookUrl.length - 1];
	}

	@Override
	protected void downloadTocEntry(String bookUrl, TocEntry tocEntry, OutputStream output, String destinationFolder)
	{
		Parser parser = null;
		
		HttpDownloader httpDownloader = new HttpCookiesGetDownloader();
		
		String[] splittedChapterUrl = tocEntry.getUrl().split("/");
		
		String chapterName = splittedChapterUrl[splittedChapterUrl.length - 1];
		
		String chapterUrl = String.format(SafBConstants.SAFB_BOOK_CHAPTER_FORMAT_STRING, bookId, chapterName);
		
		String html = httpDownloader.downloadFromUrlAsString(chapterUrl);
		
		try {
			parser = new Parser(html);
			
			NodeList hrNodes = parser.extractAllNodesThatMatch(new TagNameFilter("hr"));
			
			if (hrNodes.size() >= 2)
			{
				Tag secondHr = (Tag) hrNodes.elementAt(1);
				Div parentDiv = (Div) secondHr.getParent();
				
				int secondHrIndex = parentDiv.findPositionOf(secondHr);
				for (; secondHrIndex >= 0; secondHrIndex--)
				{
					parentDiv.removeChild(0);
				}
				
				HTMLUtils.fixListElements(parentDiv);
				HTMLUtils.fixTableElements(parentDiv);

				int lastChildIndex = parentDiv.getChildCount() - 1;
				
				if(parentDiv.getChild(lastChildIndex) instanceof ScriptTag)
					parentDiv.removeChild(lastChildIndex);
				
				getImageDownloader().downloadImagesFromHtml(SafBConstants.SAFB_BASE_URL, parentDiv, destinationFolder);
				
				output.write(parentDiv.getChildrenHTML().getBytes("UTF-8"));
			}
			else
			{
				NodeList linksToChapter = parser.extractAllNodesThatMatch(new AndFilter(new TagNameFilter("a"), new HasAttributeFilter("name", chapterName)));
				
				if(linksToChapter.size() > 0)
				{
					LinkTag linkTag = (LinkTag) linksToChapter.elementAt(0);
					
					Div parentDiv = (Div) linkTag.getParent();
					
					while(parentDiv.getChildCount() > 0 && !parentDiv.getFirstChild().equals(linkTag))
					{
						parentDiv.removeChild(0);
					}
					
					HTMLUtils.fixListElements(parentDiv);

					int lastChildIndex = parentDiv.getChildCount() - 1;
					
					if(parentDiv.getChild(lastChildIndex) instanceof ScriptTag)
						parentDiv.removeChild(lastChildIndex);
					
					getImageDownloader().downloadImagesFromHtml(SafBConstants.SAFB_BASE_URL, parentDiv, destinationFolder);
					
					output.write(parentDiv.getChildrenHTML().getBytes("UTF-8"));
				}else
				{
					parser.reset();
					NodeList epubTitleNodes = parser.extractAllNodesThatMatch(new HasAttributeFilter("class", "epub__title"));
					
					if(epubTitleNodes.size() > 0)
					{
						Tag epubTitleTag = (Tag) epubTitleNodes.elementAt(0);
						
						Div parentDiv = (Div) epubTitleTag.getParent();
						
						HTMLUtils.fixListElements(parentDiv);

						int lastChildIndex = parentDiv.getChildCount() - 1;
						
						if(parentDiv.getChild(lastChildIndex) instanceof ScriptTag)
							parentDiv.removeChild(lastChildIndex);
						
						getImageDownloader().downloadImagesFromHtml(SafBConstants.SAFB_BASE_URL, parentDiv, destinationFolder);
						
						output.write(parentDiv.getChildrenHTML().getBytes("UTF-8"));
					}
					else
					{
						parser.reset();
						
						NodeList epubBookSectionNodes = parser.extractAllNodesThatMatch(new HasAttributeFilter("class", "epub__booksection"));
						
						if(epubBookSectionNodes.size() > 0)
						{
							Tag headerTitleTag = (Tag) epubBookSectionNodes.elementAt(0);
							
							Div parentDiv = (Div) headerTitleTag;
							
							HTMLUtils.fixListElements(parentDiv);

							int lastChildIndex = parentDiv.getChildCount() - 1;
							
							if(parentDiv.getChild(lastChildIndex) instanceof ScriptTag)
								parentDiv.removeChild(lastChildIndex);
							
							getImageDownloader().downloadImagesFromHtml(SafBConstants.SAFB_BASE_URL, parentDiv, destinationFolder);
							
							output.write(parentDiv.getChildrenHTML().getBytes("UTF-8"));
						}
						else
						{
							NodeList headerTitleNodes = parser.extractAllNodesThatMatch(new HasAttributeFilter("id", chapterName));
							
							if(headerTitleNodes.size() > 0)
							{
								Tag headerTitleTag = (Tag) headerTitleNodes.elementAt(0);
								
								Div parentDiv = (Div) headerTitleTag.getParent();
								
								HTMLUtils.fixListElements(parentDiv);
		
								int lastChildIndex = parentDiv.getChildCount() - 1;
								
								if(parentDiv.getChild(lastChildIndex) instanceof ScriptTag)
									parentDiv.removeChild(lastChildIndex);
								
								getImageDownloader().downloadImagesFromHtml(SafBConstants.SAFB_BASE_URL, parentDiv, destinationFolder);
								
								output.write(parentDiv.getChildrenHTML().getBytes("UTF-8"));
							}
						}
					}
				}
			}
		} catch (ParserException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
