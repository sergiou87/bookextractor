package com.myapps.bookextractor.safari;

import java.util.ArrayList;
import java.util.List;

import org.htmlparser.Parser;
import org.htmlparser.filters.AndFilter;
import org.htmlparser.filters.HasAttributeFilter;
import org.htmlparser.filters.NotFilter;
import org.htmlparser.filters.TagNameFilter;
import org.htmlparser.tags.Div;
import org.htmlparser.tags.LinkTag;
import org.htmlparser.util.NodeList;
import org.htmlparser.util.ParserException;

import com.myapps.bookextractor.downloader.HttpCookiesGetDownloader;
import com.myapps.bookextractor.downloader.HttpDownloader;
import com.myapps.bookextractor.toc.AbstractTocExtractor;
import com.myapps.bookextractor.toc.TocEntry;

public class SafBTocExtractor extends AbstractTocExtractor {

	@Override
	protected List<TocEntry> getTocEntriesFromUrlData(String data) {
		List<TocEntry> tocEntries = null;
		
		try {
			Parser parser = new Parser(data);

			//Get the entity which contains the table of contents
			NodeList tocNodeList = parser.extractAllNodesThatMatch(new AndFilter(new TagNameFilter("div"), new HasAttributeFilter("class", "toc_book")));
			
			if(tocNodeList.size() == 1)
			{
				Div toc_book = (Div) tocNodeList.elementAt(0);
				
				NodeList tocLinks = new NodeList();
				
				//Get the actual info from the table of contents
				toc_book.collectInto(tocLinks, new AndFilter(new TagNameFilter("a"), new NotFilter(new HasAttributeFilter("title", "View Sample"))));

				//Create and fill tocEntries list
				tocEntries = new ArrayList<TocEntry>();
				
				int numTocLinks = tocLinks.size();
				
				for(int i = 0; i < numTocLinks; i++)
				{
					LinkTag linkTag = (LinkTag) tocLinks.elementAt(i);
					
					TocEntry entry = new TocEntry();
					entry.setTitle(linkTag.getLinkText());
					entry.setUrl(linkTag.getLink());
					
					tocEntries.add(entry);
				}
			}
		} catch (ParserException e) {
			e.printStackTrace();
		}
		
		return tocEntries;
	}

	@Override
	protected HttpDownloader createHttpDownloader() {
		return new HttpCookiesGetDownloader();
	}


}
