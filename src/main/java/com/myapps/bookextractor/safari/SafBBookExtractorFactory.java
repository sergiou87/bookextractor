package com.myapps.bookextractor.safari;

import org.htmlparser.Parser;
import org.htmlparser.filters.StringFilter;
import org.htmlparser.util.NodeList;
import org.htmlparser.util.ParserException;

import com.myapps.bookextractor.BookExtractor;
import com.myapps.bookextractor.DefaultBookExtractor;
import com.myapps.bookextractor.chapter.ChapterDownloader;
import com.myapps.bookextractor.downloader.HttpCookiesGetDownloader;
import com.myapps.bookextractor.downloader.HttpDownloader;
import com.myapps.bookextractor.factory.BookExtractorFactory;

public class SafBBookExtractorFactory extends BookExtractorFactory {

	@Override
	public BookExtractor createBookExtractor() {
		BookExtractor bookExtractor = new DefaultBookExtractor();
		
		bookExtractor.setBookMetadataExtractor(new SafBBookMetadataExtractor());
		bookExtractor.setTocExtractor(new SafBTocExtractor());
		
		ChapterDownloader chapterDownloader = new SafBChapterDownloader();
		chapterDownloader.setImageDownloader(new SafBImageDownloader());
		bookExtractor.setChapterDownloader(chapterDownloader);
		
		return bookExtractor;
	}

	@Override
	public boolean isValidUrl(String url) {
		return url != null && url.startsWith(SafBConstants.SAFB_BASE_URL);
	}

	@Override
	public boolean needsCookies() {
		return true;
	}

	@Override
	public boolean areCookiesValid(String url) {
		HttpDownloader httpDownloader = new HttpCookiesGetDownloader();
		
		String html = httpDownloader.downloadFromUrlAsString(url);
		
		try {
			Parser parser = new Parser(html);
			
			NodeList signedOutNodes = parser.extractAllNodesThatMatch(new StringFilter("Sign Out"));
			
			return signedOutNodes.size() != 0;

		} catch (ParserException e) {
			e.printStackTrace();
			
		}

		return false;
	}

	@Override
	public String convertToActualBookUrl(String url) {
		return url;
	}

}
