package com.myapps.bookextractor.safari;

import org.htmlparser.Parser;
import org.htmlparser.filters.HasAttributeFilter;
import org.htmlparser.filters.HasParentFilter;
import org.htmlparser.tags.Bullet;
import org.htmlparser.tags.ImageTag;
import org.htmlparser.util.NodeList;
import org.htmlparser.util.ParserException;

import com.myapps.bookextractor.downloader.HttpCookiesGetDownloader;
import com.myapps.bookextractor.downloader.HttpDownloader;
import com.myapps.bookextractor.metadata.AbstractBookMetadataExtractor;
import com.myapps.bookextractor.metadata.BookMetadata;

public class SafBBookMetadataExtractor extends AbstractBookMetadataExtractor {

	@Override
	protected HttpDownloader createHttpDownloader() {
		return new HttpCookiesGetDownloader();
	}

	@Override
	protected BookMetadata getBookMetadata(String data) {
		BookMetadata bookMetadata = new BookMetadata();
		
		try {
			Parser parser = new Parser(data);

			ImageTag bookCover = (ImageTag) parser.extractAllNodesThatMatch(new HasAttributeFilter("class", "book_cover")).elementAt(0);

			bookMetadata.setTitle(bookCover.getAttribute("title"));
			
			String coverUrl = bookCover.getImageURL();
			String[] splittedCoverUrl = coverUrl.split("/");
			String coverName = splittedCoverUrl[splittedCoverUrl.length-1];
			
			downloadCover(SafBConstants.SAFB_BASE_URL + coverUrl, coverName);
			
			bookMetadata.setCover(coverName);
			
			parser.reset();
			
			NodeList metadataList = parser.extractAllNodesThatMatch(new HasParentFilter(new HasAttributeFilter("class", "metadatalist")));
			int metadataListLength = metadataList.size();
			for(int i = 0; i < metadataListLength; i++)
			{
				Bullet bullet = (Bullet) metadataList.elementAt(i);
				String bulletText = bullet.toPlainTextString();
				
				if(bulletText.startsWith("By: "))
				{
					bookMetadata.setAuthor(bulletText.replaceAll("By: ", "").trim());
				}else if(bulletText.startsWith("Publisher: "))
				{
					bookMetadata.setPublisher(bulletText.replaceAll("Publisher: ", "").trim());
				}
			}
		} catch (ParserException e) {
			e.printStackTrace();
		}
		
		return bookMetadata;
	}
	
	private void downloadCover(String coverUrl, String coverName)
	{
		HttpDownloader httpDownloader = createHttpDownloader();
		
		httpDownloader.downloadFromUrlToFile(coverUrl, this.getDestinationFolder() + coverName);
	}

}
