package com.myapps.bookextractor.safari;

import java.util.Calendar;

import com.myapps.bookextractor.downloader.HttpCookiesGetDownloader;
import com.myapps.bookextractor.downloader.HttpDownloader;
import com.myapps.bookextractor.images.AbstractImageDownloader;

public class SafBImageDownloader extends AbstractImageDownloader {
	
	@Override
	protected HttpDownloader createHttpDownloader() {
		return new HttpCookiesGetDownloader();
	}

	@Override
	protected String getImageName(String imageUrl) {
		return String.format("image-%d-%d.jpg", imageUrl.hashCode(), Calendar.getInstance().getTimeInMillis());
	}

	@Override
	protected boolean isImageEligibleForDownload(String imageUrl) {
		return !imageUrl.endsWith(".gif");
	}

	@Override
	protected String getImageUrl(String baseUrl, String imageSrc)
	{
		return baseUrl + imageSrc;
	}

}
