package com.myapps.bookextractor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;

import com.myapps.bookextractor.chapter.ChapterDownloader;
import com.myapps.bookextractor.metadata.BookMetadata;
import com.myapps.bookextractor.metadata.BookMetadataExtractor;
import com.myapps.bookextractor.toc.TocEntry;
import com.myapps.bookextractor.toc.TocExtractor;

/**
 * Default implementation for book extractor
 * @author Sergio
 *
 */
public class DefaultBookExtractor implements BookExtractor {
	
	/**
	 * Table of contents extractor
	 */
	private TocExtractor tocExtractor;
	
	/**
	 * Chapter downloader
	 */
	private ChapterDownloader chapterDownloader;
	
	/**
	 * Book metadata extractor
	 */
	private BookMetadataExtractor bookMetadataExtractor;
	
	/**
	 * Book metadata
	 */
	private BookMetadata bookMetadata;

	private String destinationFolder;

	public void downloadBookFromUrl(String bookUrl, String destinationHtmlFile)
	{
		int lastPathSeparator = destinationHtmlFile.lastIndexOf(File.separator);
		destinationFolder = lastPathSeparator == -1 ? "" : destinationHtmlFile.substring(0, lastPathSeparator + 1);

		bookMetadata = bookMetadataExtractor.downloadBookMetadata(bookUrl, destinationFolder);

		List<TocEntry> tocEntries = tocExtractor.extractTableOfContentsFromUrl(bookUrl);
		FileOutputStream fos;
		try {
			fos = new FileOutputStream(destinationHtmlFile);
			
			insertHtmlBeginning(fos);
			
			chapterDownloader.downloadTocEntries(bookUrl, tocEntries, fos, destinationFolder);
			
			insertHtmlEnd(fos);

			fos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Inserts some HTML at the end of the output file
	 * @param fos Output file
	 */
	protected void insertHtmlEnd(OutputStream fos)
	{
		String htmlEndText = "</body></html>";
		
		try {
			fos.write(htmlEndText.getBytes("utf-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Inserts some HTML at the beginning of the output file.
	 * @param fos Output file
	 */
	protected void insertHtmlBeginning(OutputStream fos)
	{
		String htmlBeginningText = "<html><head>" +
				"<title>" + bookMetadata.getTitle() + "</title>" +
				"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />" + 
				"<meta name=\"Author\" content=\"" + bookMetadata.getAuthor() + "\" />" + 
				"<meta name=\"Publisher\" content=\"" + bookMetadata.getPublisher() + "\" />" + 
						"</head><body>";
		
		if(!"".equals(bookMetadata.getCover()))
			htmlBeginningText += "<img src=\"" + bookMetadata.getCover() + "\" />";
		
		try {
			fos.write(htmlBeginningText.getBytes("utf-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void setTocExtractor(TocExtractor tocExtractor) {
		this.tocExtractor = tocExtractor;
	}

	public void setChapterDownloader(ChapterDownloader chapterDownloader) {
		this.chapterDownloader = chapterDownloader;		
	}

	public void setBookMetadataExtractor(
			BookMetadataExtractor bookMetadataExtractor) {
		this.bookMetadataExtractor = bookMetadataExtractor;
	}

	public TocExtractor getTocExtractor() {
		return tocExtractor;
	}

	public ChapterDownloader getChapterDownloader() {
		return chapterDownloader;
	}

	public BookMetadataExtractor getBookMetadataExtractor() {
		return bookMetadataExtractor;
	}
	
}
