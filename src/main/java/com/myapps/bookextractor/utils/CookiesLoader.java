package com.myapps.bookextractor.utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

import org.apache.commons.httpclient.Cookie;
import org.apache.commons.httpclient.HttpClient;

public class CookiesLoader {
	
	public static final String COOKIES_FILE = "cookies.txt";
	
	//Method based on FetchHTTP.loadCookies from http://crawler.archive.org/xref/org/archive/crawler/fetcher/FetchHTTP.html
	/***
	* Load cookies from a file before the first fetch.
	* <p>
	* The file is a text file in the Netscape's 'cookies.txt' file format.<br>
	* Example entry of cookies.txt file:<br>
	* <br>
	* www.archive.org FALSE / FALSE 1074567117 details-visit texts-cralond<br>
	* <br>
	* Each line has 7 tab-separated fields:<br>
	* <li>1. DOMAIN: The domain that created and have access to the cookie
	* value.
	* <li>2. FLAG: A TRUE or FALSE value indicating if hosts within the given
	* domain can access the cookie value.
	* <li>3. PATH: The path within the domain that the cookie value is valid
	* for.
	* <li>4. SECURE: A TRUE or FALSE value indicating if to use a secure
	* connection to access the cookie value.
	* <li>5. EXPIRATION: The expiration time of the cookie value (unix style.)
	* <li>6. NAME: The name of the cookie value
	* <li>7. VALUE: The cookie value
	*
	* @param cookiesFile file in the Netscape's 'cookies.txt' format.
	*/
	public static void loadCookies(HttpClient http, String cookiesFile) {
	    // Do nothing if cookiesFile is not specified.
	    if (cookiesFile == null || cookiesFile.length() <= 0) {
	        return;
	    }
	    RandomAccessFile raf = null;
	    try {
	        raf = new RandomAccessFile(cookiesFile, "r");
	        String[] cookieParts;
	        String line;
	        Cookie cookie = null;
	        while ((line = raf.readLine()) != null) {
	            // Line that starts with # is commented line, therefore skip it.
	            if (!line.startsWith("#")) {
	                cookieParts = line.split("\\t");
	                if (cookieParts.length == 7) {
	                    // Create cookie with not expiration date (-1 value).
	                    // TODO: add this as an option.
	                    cookie =
	                        new Cookie(cookieParts[0], cookieParts[5],
	                            cookieParts[6], cookieParts[2], -1,
	                            Boolean.valueOf(cookieParts[3]).booleanValue());

	                    if (cookieParts[1].toLowerCase().equals("true")) {
	                        cookie.setDomainAttributeSpecified(true);
	                    } else {
	                        cookie.setDomainAttributeSpecified(false);
	                    }
	                    http.getState().addCookie(cookie);
	                }
	            }
	        }
	    } catch (FileNotFoundException e) {
	        // We should probably throw FatalConfigurationException.
	    	e.printStackTrace();
	    } catch (IOException e) {
	        // We should probably throw FatalConfigurationException.
	        e.printStackTrace();
	    } finally {
	        try {
	            if (raf != null) {
	                raf.close();
	            }
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	    }
	}

	/*
	public static void loadCookiesFromFile(HttpClient http, String cookiesFile)
	{

	}
	*/
}
