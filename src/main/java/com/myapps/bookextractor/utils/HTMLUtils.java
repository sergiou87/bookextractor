package com.myapps.bookextractor.utils;

import org.htmlparser.Node;
import org.htmlparser.filters.AndFilter;
import org.htmlparser.filters.HasChildFilter;
import org.htmlparser.filters.TagNameFilter;
import org.htmlparser.tags.Bullet;
import org.htmlparser.tags.ParagraphTag;
import org.htmlparser.tags.TableColumn;
import org.htmlparser.tags.TableRow;
import org.htmlparser.tags.TableTag;
import org.htmlparser.util.NodeList;

public class HTMLUtils {
	/**
	 * Books from this server have elements like <li><p...>...</p>...</li> that are
	 * not shown right in Kindle, so this method removes the p tags of the first element.
	 * @param node HTML node containing the nodes to fix
	 */
	public static void fixListElements(Node node)
	{
		NodeList listNodes = new NodeList();
		node.collectInto(listNodes, new AndFilter(new TagNameFilter("li"), new HasChildFilter(new TagNameFilter("p"))));
		
		int listNodesCount = listNodes.size();
		for(int i = 0; i < listNodesCount; i++)
		{
			Bullet bullet = (Bullet) listNodes.elementAt(i);
			
			if(bullet.getChild(0) instanceof ParagraphTag)
			{
				ParagraphTag p = (ParagraphTag) bullet.getChild(0);
				bullet.setChildren(p.getChildren());
			}
		}
	}
	
	public static void fixTableElements(Node node)
	{
		NodeList listNodes = new NodeList();
		node.collectInto(listNodes, new TagNameFilter("table"));
		
		int listNodesCount = listNodes.size();
		for(int i = 0; i < listNodesCount; i++)
		{
			TableTag table = (TableTag) listNodes.elementAt(i);
			
			if (table.getChildCount() == 1 && table.getChild(0) instanceof TableRow)
			{
				TableRow tableRow = (TableRow) table.getChild(0);
				
				if (tableRow.getChildCount() == 1 && tableRow.getChild(0) instanceof TableColumn)
				{
					TableColumn tableColumn = (TableColumn) tableRow.getChild(0);
					
					Node tableParent = table.getParent();
					NodeList tableSiblings = tableParent.getChildren();
					
					int tableIndex = tableSiblings.indexOf(table);
					
					// Replace table with its children
					NodeList newChildren = new NodeList();
					for (int j = 0; j < tableSiblings.size(); j++)
					{
						if (j == tableIndex)
							newChildren.add(tableColumn.getChildren());
						else
							newChildren.add(tableSiblings.elementAt(j));
					}
					tableParent.setChildren(newChildren);
				}
			}
		}
	}

}
