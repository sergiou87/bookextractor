package com.myapps.bookextractor.gui;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.Observable;
import java.util.Observer;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;

import org.apache.commons.io.IOUtils;

import com.myapps.bookextractor.BookExtractor;
import com.myapps.bookextractor.chapter.ChapterDownloader;
import com.myapps.bookextractor.factory.BookExtractorFactory;


public class ApplicationWindow implements Observer {

	private JFrame frmBookExtractor;
	private JTextField txtBookURL;
	private JTextArea txtCookies;
	private JProgressBar progressBar;
	private JButton btnBrowse;
	private JTextField txtDestinationFolder;
	private JButton btnDownload;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ApplicationWindow window = new ApplicationWindow();
					window.frmBookExtractor.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ApplicationWindow() {
		initialize();
		
		loadCookies();
	}

	private void loadCookies() {
		try {
			FileInputStream fis = new FileInputStream("cookies.txt");
			
			StringWriter writer = new StringWriter();
			IOUtils.copy(fis, writer);
			txtCookies.setText(writer.toString());
			
			fis.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void saveCookies() {
		try {
			FileOutputStream fos = new FileOutputStream("cookies.txt");
			
			fos.write(txtCookies.getText().getBytes());
			
			fos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmBookExtractor = new JFrame();
		frmBookExtractor.setTitle("Book Extractor");
		frmBookExtractor.setBounds(100, 100, 450, 435);
		frmBookExtractor.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JLabel lblNewLabel = new JLabel("Book URL:");
		
		txtBookURL = new JTextField();
		txtBookURL.setColumns(10);
		
		JLabel lblCookies = new JLabel("Cookies:");
		
		btnDownload = new JButton("Download");
		btnDownload.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				downloadAndConvert();
			}
		});
		
		JButton btnSaveCookies = new JButton("Save cookies");
		btnSaveCookies.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				saveCookies();
			}
		});
		
		JScrollPane scrollPane = new JScrollPane();
		
		progressBar = new JProgressBar();
		
		JLabel lblDestinationFolder = new JLabel("Destination folder:");
		
		txtDestinationFolder = new JTextField();
		txtDestinationFolder.setEditable(false);
		txtDestinationFolder.setColumns(10);
		
		btnBrowse = new JButton("Browse");
		btnBrowse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				browseDestinationFolder();
			}
		});
		GroupLayout groupLayout = new GroupLayout(frmBookExtractor.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(progressBar, GroupLayout.DEFAULT_SIZE, 414, Short.MAX_VALUE)
						.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 414, Short.MAX_VALUE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblDestinationFolder)
								.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 88, GroupLayout.PREFERRED_SIZE))
							.addGap(18)
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addComponent(btnDownload)
								.addComponent(txtBookURL, GroupLayout.DEFAULT_SIZE, 307, Short.MAX_VALUE)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(txtDestinationFolder, GroupLayout.DEFAULT_SIZE, 234, Short.MAX_VALUE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(btnBrowse))))
						.addComponent(lblCookies)
						.addComponent(btnSaveCookies))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(txtBookURL, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblNewLabel))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblDestinationFolder)
						.addComponent(txtDestinationFolder, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnBrowse))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnDownload)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(progressBar, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblCookies)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 216, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnSaveCookies, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		
		txtCookies = new JTextArea();
		scrollPane.setViewportView(txtCookies);
		frmBookExtractor.getContentPane().setLayout(groupLayout);
	}

	protected void downloadAndConvert() {
		String bookUrl = txtBookURL.getText();
		final String destinationFolder = txtDestinationFolder.getText();
		
		try {
			new URL(bookUrl);
		} catch (MalformedURLException e) {
			JOptionPane.showMessageDialog(txtBookURL, "Book Url is not valid.", "Error", JOptionPane.ERROR_MESSAGE);
			
			return;
		}
		
		if("".equals(destinationFolder))
		{
			JOptionPane.showMessageDialog(txtDestinationFolder, "Select a valid destination folder.", "Error", JOptionPane.ERROR_MESSAGE);
			
			return;
		}

		final String destinationHtmlFile = Paths.get(destinationFolder, "book.html").toString();
		
		final BookExtractorFactory factory = BookExtractorFactory.createBookExtractorFactoryForUrl(bookUrl);
		
		if(factory == null)
		{
			JOptionPane.showMessageDialog(btnDownload, "There is no book extractor for the selected Url.", "Error", JOptionPane.ERROR_MESSAGE);
			
			return;
		}
		
		final String actualBookUrl = factory.convertToActualBookUrl(bookUrl);
		
		if(actualBookUrl == null)
		{
			JOptionPane.showMessageDialog(btnDownload, "It seems that the given Url is not valid.", "Error", JOptionPane.ERROR_MESSAGE);
			
			return;
		}
		
		if(factory.needsCookies() && !factory.areCookiesValid(actualBookUrl))
		{
			JOptionPane.showMessageDialog(btnDownload, "This book service requires valid cookies, but your cookies are not valid. Please, supply valid cookies and save them.", "Error", JOptionPane.ERROR_MESSAGE);
			
			return;
		}
		
		final String prevButtonText = btnDownload.getText();
		btnDownload.setText("Downloading...");
		btnDownload.setEnabled(false);
		progressBar.setValue(0);

		final BookExtractor bookExtractor = factory.createBookExtractor();
		
		if(bookExtractor.getChapterDownloader() instanceof Observable)
		{
			Observable observableChapterDownloader = (Observable) bookExtractor.getChapterDownloader();
			observableChapterDownloader.addObserver(this);
		}
		
		new Thread() {			
			public void run() {
				bookExtractor.downloadBookFromUrl(actualBookUrl, destinationHtmlFile);
				
				btnDownload.setText(prevButtonText);
				btnDownload.setEnabled(true);
			}
		}.start();
	}

	protected void browseDestinationFolder() {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		fileChooser.setVisible(true);
		
		int result = fileChooser.showOpenDialog(txtDestinationFolder);
		
		if(result == JFileChooser.APPROVE_OPTION)
		{
			txtDestinationFolder.setText(fileChooser.getSelectedFile().getAbsolutePath());
		}
	}

	public void update(Observable o, Object arg)
	{
		if((o instanceof ChapterDownloader) && (arg instanceof Integer))
		{
			progressBar.setValue((Integer) arg);
		}
	}
}
