package com.myapps.bookextractor.metadata;

/**
 * Interface for book metadata extractors
 * @author Sergio
 *
 */
public interface BookMetadataExtractor {
	
	/**
	 * Downloads the book metadata.
	 * @param bookUrl Book URL
	 * @param destinationFolder Destination folder, needed to download the book cover
	 * @return Book metadata
	 */
	BookMetadata downloadBookMetadata(String bookUrl, String destinationFolder);
}
