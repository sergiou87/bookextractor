package com.myapps.bookextractor.metadata;

import com.myapps.bookextractor.downloader.HttpDownloader;

/**
 * Abstract template for book metadata extractors
 * @author Sergio
 *
 */
public abstract class AbstractBookMetadataExtractor implements
		BookMetadataExtractor {
	
	private String destinationFolder;

	public BookMetadata downloadBookMetadata(String bookUrl, String destinationFolder) {
		this.destinationFolder = destinationFolder;
		
		HttpDownloader httpDownloader = createHttpDownloader();
		
		String data = httpDownloader.downloadFromUrlAsString(bookUrl);

		BookMetadata bookMetadata = getBookMetadata(data);
		
		return bookMetadata;
	}
	
	/**
	 * Creates a suitable HTTP downloader.
	 * @return HTTP downloader
	 */
	protected abstract HttpDownloader createHttpDownloader();

	/**
	 * Extracts the book metadata from the data obtained from the url
	 * @param data Data from the url
	 * @return Book metadata
	 */
	protected abstract BookMetadata getBookMetadata(String data);

	public String getDestinationFolder() {
		return destinationFolder;
	}

	public void setDestinationFolder(String destinationFolder) {
		this.destinationFolder = destinationFolder;
	}

}
