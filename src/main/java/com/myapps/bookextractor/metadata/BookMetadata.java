package com.myapps.bookextractor.metadata;

/**
 * Book metadata
 * @author Sergio
 *
 */
public class BookMetadata {

	/**
	 * Title
	 */
	private String title = "";
	
	/**
	 * Author
	 */
	private String author = "";
	
	/**
	 * Publisher
	 */
	private String publisher = "";
	
	/**
	 * Cover filename
	 */
	private String cover = "";
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getAuthor() {
		return author;
	}
	
	public void setAuthor(String author) {
		this.author = author;
	}
	
	public String getPublisher() {
		return publisher;
	}
	
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	
	public String getCover() {
		return cover;
	}

	public void setCover(String cover) {
		this.cover = cover;
	}

	@Override
	public String toString() {
		return String.format("[title = %s, author = %s, publisher = %s]", title, author, publisher);
	}
	
	
}
