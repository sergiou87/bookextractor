package com.myapps.bookextractor;

import com.myapps.bookextractor.chapter.ChapterDownloader;
import com.myapps.bookextractor.metadata.BookMetadataExtractor;
import com.myapps.bookextractor.toc.TocExtractor;

/**
 * Interface for book extractors.
 * @author Sergio
 *
 */
public interface BookExtractor {
	
	/**
	 * Download a book from an URL and save it into an HTML file.
	 * @param bookUrl Url of the book
	 * @param destinationHtmlFile Destination HTML file
	 */
	void downloadBookFromUrl(String bookUrl, String destinationHtmlFile);
	
	/**
	 * Set the book metadata extractor.
	 * @param bookMetadataExtractor Book metadata extractor.
	 */
	void setBookMetadataExtractor(BookMetadataExtractor bookMetadataExtractor);
	BookMetadataExtractor getBookMetadataExtractor();
	
	/**
	 * Set the Table of Contents extractor
	 * @param tocExtractor Table of contents extractor
	 */
	void setTocExtractor(TocExtractor tocExtractor);
	TocExtractor getTocExtractor();
	
	/**
	 * Set the chapter downloader
	 * @param chapterDownloader Chapter downloader
	 */
	void setChapterDownloader(ChapterDownloader chapterDownloader);
	ChapterDownloader getChapterDownloader();
}
