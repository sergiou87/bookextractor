package com.myapps.bookextractor.appl;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.myapps.bookextractor.downloader.DefaultHttpGetDownloader;
import com.myapps.bookextractor.downloader.HttpDownloader;
import com.myapps.bookextractor.toc.AbstractTocExtractor;
import com.myapps.bookextractor.toc.TocEntry;

public class ApplTocExtractor extends AbstractTocExtractor {
	
	@Override
	public List<TocEntry> extractTableOfContentsFromUrl(String url) {
		return super.extractTableOfContentsFromUrl(url + "/book.json");
	}

	@Override
	protected HttpDownloader createHttpDownloader() {
		return new DefaultHttpGetDownloader();
	}

	@Override
	protected List<TocEntry> getTocEntriesFromUrlData(String data) {
		List<TocEntry> entries = new ArrayList<TocEntry>();
		
		JSONObject bookJson = (JSONObject) JSONValue.parse(data);
		
		JSONArray sections = (JSONArray) bookJson.get("sections");
		
		if(sections != null)
			getTocEntriesFromSections(entries, sections);

		/*
		try {
			NodeList tocLinks = parser.extractAllNodesThatMatch(new TagNameFilter("a"));//new AndFilter(new TagNameFilter("a"), new HasParentFilter(new AndFilter(new TagNameFilter("ul"), new HasAttributeFilter("id", "toc")), false)));
			
			int tocLinksCount = tocLinks.size();
			for(int i = 0; i < tocLinksCount; i++)
			{
				LinkTag linkTag = (LinkTag) tocLinks.elementAt(i);
				
				TocEntry entry = new TocEntry();
				entry.setTitle(linkTag.getText());
				entry.setUrl(linkTag.getLink());
				entries.add(entry);
			}
		} catch (ParserException e) {
			e.printStackTrace();
		}
		*/
		System.out.println(entries);
		
		return entries;
	}

	private void getTocEntriesFromSections(List<TocEntry> entries,
			JSONArray sections) {
		for(int i = 0; i < sections.size(); i++)
		{
			JSONObject section = (JSONObject) sections.get(i);
			
			TocEntry entry = new TocEntry();
			entry.setTitle((String) section.get("title"));
			entry.setUrl(cleanReferenceFromUrl((String) section.get("href")));
			
			if(!isTocEntryDuplicated(entry, entries))
				entries.add(entry);

			JSONArray childrenSections = (JSONArray) section.get("sections");
			
			if(childrenSections != null)
				getTocEntriesFromSections(entries, childrenSections);
		}
	}
	
	private String cleanReferenceFromUrl(String url)
	{
		int sepIndex = url.lastIndexOf("#");
		return url.substring(0, sepIndex);
	}
	
	private boolean isTocEntryDuplicated(TocEntry entry, List<TocEntry> entries)
	{
		for(TocEntry te : entries)
			if(entry.getUrl().equals(te.getUrl()))
				return true;
		
		return false;
	}

}
