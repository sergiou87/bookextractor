package com.myapps.bookextractor.appl;

import java.util.Calendar;

import com.myapps.bookextractor.downloader.DefaultHttpGetDownloader;
import com.myapps.bookextractor.downloader.HttpDownloader;
import com.myapps.bookextractor.images.AbstractImageDownloader;

public class ApplImageDownloader extends AbstractImageDownloader {

//url del libro + /Art/nombreimagen (src de la imagen - .. al principio)

	@Override
	protected HttpDownloader createHttpDownloader() {
		return new DefaultHttpGetDownloader();
	}

	@Override
	protected String getImageName(String imageUrl) {
		return String.format("image-%d-%d.jpg", imageUrl.hashCode(), Calendar.getInstance().getTimeInMillis());
	}

	@Override
	protected boolean isImageEligibleForDownload(String imageUrl) {
		return true;
	}

	@Override
	protected String getImageUrl(String baseUrl, String imageSrc) {
		imageSrc = imageSrc.replace("..", "");
		
		return baseUrl + imageSrc;
	}

}
