package com.myapps.bookextractor.appl;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.myapps.bookextractor.downloader.DefaultHttpGetDownloader;
import com.myapps.bookextractor.downloader.HttpDownloader;
import com.myapps.bookextractor.metadata.AbstractBookMetadataExtractor;
import com.myapps.bookextractor.metadata.BookMetadata;

public class ApplBookMetadataExtractor extends AbstractBookMetadataExtractor {

	@Override
	public BookMetadata downloadBookMetadata(String bookUrl,
			String destinationFolder) {
		return super.downloadBookMetadata(bookUrl + "/book.json", destinationFolder);
	}

	@Override
	protected HttpDownloader createHttpDownloader() {
		return new DefaultHttpGetDownloader();
	}

	@Override
	protected BookMetadata getBookMetadata(String data) {
		BookMetadata bookMetadata = new BookMetadata();
		
		JSONObject bookJson = (JSONObject) JSONValue.parse(data);
		
		bookMetadata.setTitle((String) bookJson.get("title"));
		bookMetadata.setAuthor(ApplConstants.APPL_NAME);
		bookMetadata.setPublisher(ApplConstants.APPL_NAME);
		bookMetadata.setCover("");

		return bookMetadata;
	}

}
