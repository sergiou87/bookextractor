package com.myapps.bookextractor.appl;

import java.net.MalformedURLException;
import java.net.URL;

import com.myapps.bookextractor.BookExtractor;
import com.myapps.bookextractor.DefaultBookExtractor;
import com.myapps.bookextractor.chapter.ChapterDownloader;
import com.myapps.bookextractor.factory.BookExtractorFactory;

public class ApplBookExtractorFactory extends BookExtractorFactory {

	@Override
	public BookExtractor createBookExtractor() {
		BookExtractor bookExtractor = new DefaultBookExtractor();
		
		bookExtractor.setBookMetadataExtractor(new ApplBookMetadataExtractor());
		bookExtractor.setTocExtractor(new ApplTocExtractor());

		ChapterDownloader chapterDownloader = new ApplChapterDownloader();
		chapterDownloader.setImageDownloader(new ApplImageDownloader());
		bookExtractor.setChapterDownloader(chapterDownloader);
		
		return bookExtractor;
	}

	@Override
	public boolean isValidUrl(String url) {
		return url.startsWith(ApplConstants.APPL_BASE_URL + "/library");
	}

	@Override
	public boolean needsCookies() {
		return false;
	}

	@Override
	public boolean areCookiesValid(String url) {
		return true;
	}

	/**
	 * An actual book URL from this service must have the next format:<br/>
	 * http://www.mydomain.com/library/some/documentation/Something/Another/Theme<br/>
	 * Protocol://domain/path1/path2/path3/path4/path5/path6 (exactly 6 path components)
	 */
	@Override
	public String convertToActualBookUrl(String url) {
		final int ACTUAL_BOOK_PATH_COMPONENTS = 6;
		
		//When urls from this service are obtained from a browser,
		//you can get a #documentation path component which should be "fixed"
		url = url.replace("/#documentation", "/documentation");

		try {
			URL urlObject = new URL(url);

			String[] pathComponents = urlObject.getPath().split("/");
			
			//Not enough path components
			if(pathComponents.length < ACTUAL_BOOK_PATH_COMPONENTS+1)
				return null;
			
			String path = "";
			for(int i = 1; i <= ACTUAL_BOOK_PATH_COMPONENTS; i++)
				path += "/" + pathComponents[i];
			
			url = urlObject.getProtocol() + "://" + urlObject.getHost() + path;
		} catch (MalformedURLException e) {
			e.printStackTrace();
			
			return null;
		}
		
		return url;
	}

}
