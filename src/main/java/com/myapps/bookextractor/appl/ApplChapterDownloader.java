package com.myapps.bookextractor.appl;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;

import org.htmlparser.Node;
import org.htmlparser.Parser;
import org.htmlparser.filters.HasAttributeFilter;
import org.htmlparser.nodes.TagNode;
import org.htmlparser.util.NodeList;
import org.htmlparser.util.ParserException;

import com.myapps.bookextractor.chapter.AbstractChapterDownloader;
import com.myapps.bookextractor.downloader.HttpCookiesGetDownloader;
import com.myapps.bookextractor.downloader.HttpDownloader;
import com.myapps.bookextractor.toc.TocEntry;
import com.myapps.bookextractor.utils.HTMLUtils;

public class ApplChapterDownloader extends AbstractChapterDownloader {

	@Override
	protected void downloadTocEntry(String bookUrl, TocEntry tocEntry,
			OutputStream output, String destinationFolder)
	{
		Parser parser = null;
		
		HttpDownloader httpDownloader = new HttpCookiesGetDownloader();
		
		String chapterUrl = bookUrl + "/" + tocEntry.getUrl();
		
		String html = httpDownloader.downloadFromUrlAsString(chapterUrl);
		
		try {
			parser = new Parser(html);
			
			NodeList articleContentList = new NodeList();
			TagNode pageTitle = (TagNode) parser.extractAllNodesThatMatch(new HasAttributeFilter("id", "pageTitle")).elementAt(0);

			HTMLUtils.fixListElements(pageTitle.getParent());
			HTMLUtils.fixTableElements(pageTitle.getParent());

			getImageDownloader().downloadImagesFromHtml(bookUrl, pageTitle.getParent(), destinationFolder);

			Node currentNode = pageTitle;
			
			do
			{
				if((currentNode instanceof TagNode) &&
						"pageNavigationLinks_bottom".equals(((TagNode) currentNode).getAttribute("id")))
				{
					break;
				}
				
				articleContentList.add(currentNode);
				currentNode = currentNode.getNextSibling();
			}while(currentNode != null);

			output.write(articleContentList.toHtml().getBytes("UTF-8"));
		} catch (ParserException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
