package com.myapps.bookextractor.downloader;

/**
 * Http downloader
 * @author Sergio
 *
 */
public interface HttpDownloader {

	/**
	 * Downloads the content of an URL and returns it as a String
	 * @param url Url to be downloaded
	 * @return Content of the URL
	 */
	String downloadFromUrlAsString(String url);
	
	/**
	 * Downloads the content of an URL and writes it into a file.
	 * @param url Url to ve downloaded
	 * @param filename Filename to download to
	 */
	void downloadFromUrlToFile(String url, String filename);
}
