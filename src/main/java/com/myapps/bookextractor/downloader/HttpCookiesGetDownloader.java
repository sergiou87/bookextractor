package com.myapps.bookextractor.downloader;

import org.apache.commons.httpclient.HttpClient;

import com.myapps.bookextractor.utils.CookiesLoader;

/**
 * HTTP downloader with GET method that loads cookies from a cookies.txt file (Netscape - wget format)
 * @author Sergio
 *
 */
public class HttpCookiesGetDownloader extends DefaultHttpGetDownloader {

	@Override
	protected HttpClient prepareHttpClient() {
		HttpClient client = super.prepareHttpClient();
		
		CookiesLoader.loadCookies(client, CookiesLoader.COOKIES_FILE);
		
		return client;
	}
	
}
