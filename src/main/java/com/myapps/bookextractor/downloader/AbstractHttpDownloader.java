package com.myapps.bookextractor.downloader;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;

/**
 * Abstract template for HTTP downloader
 * @author Sergio
 *
 */
public abstract class AbstractHttpDownloader implements HttpDownloader {

	public String downloadFromUrlAsString(String url)
	{
		HttpClient httpClient = prepareHttpClient();
		
		HttpMethod method = prepareHttpMethod(url);
		
		String responseBody = doRequestAsString(httpClient, method);
		
		return responseBody;
	}
	
	public void downloadFromUrlToFile(String url, String filename) {
		HttpClient httpClient = prepareHttpClient();
		
		HttpMethod method = prepareHttpMethod(url);
		
		doRequestToFile(httpClient, method, filename);
	}

	/**
	 * Prepares an HttpClient
	 * @return Prepared HttpClient
	 */
	protected abstract HttpClient prepareHttpClient();
	
	/**
	 * Prepares a HttpMethod for a given URL
	 * @param url URL
	 * @return Prepared HttpMethod
	 */
	protected abstract HttpMethod prepareHttpMethod(String url);
	
	/**
	 * Makes the actual HTTP request and returns the response body as a String
	 * @param client Http client
	 * @param method Http method
	 * @return Response body as a String
	 */
	protected abstract String doRequestAsString(HttpClient client, HttpMethod method);
	
	/**
	 * Makes the actual HTTP request and writes the response body into a file
	 * @param client Http client
	 * @param method Http method
	 * @param filename Filename where the response body will be written into
	 */
	protected abstract void doRequestToFile(HttpClient client, HttpMethod method, String filename);

}
