package com.myapps.bookextractor.downloader;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.cookie.CookiePolicy;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.io.IOUtils;

/**
 * Default implementation for HTTP downloader using GET method.
 * @author Sergio
 *
 */
public class DefaultHttpGetDownloader extends AbstractHttpDownloader {

	@Override
	protected HttpClient prepareHttpClient() {

		HttpClient client = new HttpClient();

		client.getParams().setParameter(HttpMethodParams.SINGLE_COOKIE_HEADER,
				new Boolean(true));
		
		client.getParams().setParameter(
				HttpMethodParams.UNAMBIGUOUS_STATUS_LINE, new Boolean(false));
		client.getParams().setParameter(
				HttpMethodParams.STRICT_TRANSFER_ENCODING, new Boolean(false));
		client.getParams().setIntParameter(
				HttpMethodParams.STATUS_LINE_GARBAGE_LIMIT, 10);

		return client;
	}
	
	@Override
	protected HttpMethod prepareHttpMethod(String url) {
		HttpMethod method = new GetMethod(url);

		method.getParams().setParameter(HttpMethodParams.USER_AGENT,
						"Mozilla/5.001 (windows; U; NT4.0; en-US; rv:1.0) Gecko/25250101");
		method.getParams().setParameter(HttpMethodParams.COOKIE_POLICY,
				CookiePolicy.BROWSER_COMPATIBILITY);
		method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER,
				new DefaultHttpMethodRetryHandler(3, false));
		method.setRequestHeader("Cache-Control", "no-cache");

		return method;
	}

	@Override
	protected String doRequestAsString(HttpClient client, HttpMethod method) {
		String responseBody = null;
		
		try {
			int statusCode = client.executeMethod(method);

			if (statusCode != HttpStatus.SC_OK) {
				System.out.println("Method failed: " + method.getStatusLine());
			} else {
				InputStream inputStream = method.getResponseBodyAsStream();

				StringWriter writer = new StringWriter();
				IOUtils.copy(inputStream, writer, "utf-8");// method.getResponseHeader("character-encoding").getValue());
				responseBody = writer.toString();
			}
		} catch (HttpException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			method.releaseConnection();
		}
		
		return responseBody;
	}

	@Override
	protected void doRequestToFile(HttpClient client, HttpMethod method,
			String filename) {
		try {
			int statusCode = client.executeMethod(method);

			if (statusCode != HttpStatus.SC_OK) {
				System.out.println("Method failed: " + method.getStatusLine());
			} else {
				InputStream inputStream = method.getResponseBodyAsStream();

				FileOutputStream fos = new FileOutputStream(filename);
				IOUtils.copy(inputStream, fos);
			}
		} catch (HttpException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			method.releaseConnection();
		}		
	}

}
