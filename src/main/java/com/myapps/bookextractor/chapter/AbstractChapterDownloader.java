package com.myapps.bookextractor.chapter;

import java.io.OutputStream;
import java.util.List;
import java.util.Observable;

import com.myapps.bookextractor.images.ImageDownloader;
import com.myapps.bookextractor.toc.TocEntry;

/**
 * Abstract template for chapter downloader
 * @author Sergio
 *
 */
public abstract class AbstractChapterDownloader extends Observable implements ChapterDownloader {
	private ImageDownloader imageDownloader = null;

	public void downloadTocEntries(String bookUrl, List<TocEntry> tocEntries,
			OutputStream output, String destinationFolder) {
		
		setProgress(0);
		
		int count = 0;
		
		for(TocEntry tocEntry : tocEntries)
		{
			downloadTocEntry(bookUrl, tocEntry, output, destinationFolder);

			count++;
			
			setProgress(count * 100 / tocEntries.size());
		}

		setProgress(100);
	}
	
	private void setProgress(int progress)
	{
		setChanged();
		notifyObservers(Integer.valueOf(progress));
	}
	
	public ImageDownloader getImageDownloader() {
		return imageDownloader;
	}

	public void setImageDownloader(ImageDownloader imageDownloader)
	{
		this.imageDownloader = imageDownloader;
	}
	
	/**
	 * Actual downloader of a ToC entry
	 * @param bookUrl URL of the book
	 * @param tocEntry ToC entry to be downloaded
	 * @param output Output stream where the chapters will be written into
	 * @param destinationFolder Destination folder for downloaded images
	 */
	protected abstract void downloadTocEntry(String bookUrl, TocEntry tocEntry, OutputStream output, String destinationFolder);

}
