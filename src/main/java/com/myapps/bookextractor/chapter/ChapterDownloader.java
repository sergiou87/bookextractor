package com.myapps.bookextractor.chapter;

import java.io.OutputStream;
import java.util.List;

import com.myapps.bookextractor.images.ImageDownloader;
import com.myapps.bookextractor.toc.TocEntry;

/**
 * Interface for chapter downloader
 * @author Sergio
 *
 */
public interface ChapterDownloader {
	
	/**
	 * Downloads ToC entries
	 * @param bookUrl URL of the book
	 * @param tocEntries List of ToC entries to download
	 * @param output Output to write each chapter/section
	 * @param destinationFolder Destination folder where the images will be downloaded
	 */
	void downloadTocEntries(String bookUrl, List<TocEntry> tocEntries, OutputStream output, String destinationFolder);
	
	/**
	 * Sets the image downloader
	 * @param imageDownloader Image downloader
	 */
	void setImageDownloader(ImageDownloader imageDownloader);
	ImageDownloader getImageDownloader();
}
