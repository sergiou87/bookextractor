package com.myapps.bookextractor.toc;

import java.util.List;

import com.myapps.bookextractor.downloader.HttpDownloader;

/**
 * Abstract template for ToC extractor
 * @author Sergio
 *
 */
public abstract class AbstractTocExtractor implements TocExtractor {

	public List<TocEntry> extractTableOfContentsFromUrl(String url) {
		HttpDownloader httpDownloader = createHttpDownloader();
		
		String data = httpDownloader.downloadFromUrlAsString(url);

		List<TocEntry> tocEntries = getTocEntriesFromUrlData(data);
		
		return tocEntries;
	}
	
	/**
	 * Creates a suitable Http downloader
	 * @return Http downloader
	 */
	protected abstract HttpDownloader createHttpDownloader();

	/**
	 * Extracts the ToC entries from the data.
	 * @param data Data obtained from the Url
	 * @return List of ToC entries
	 */
	protected abstract List<TocEntry> getTocEntriesFromUrlData(String data);

}
