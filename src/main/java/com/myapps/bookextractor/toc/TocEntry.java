package com.myapps.bookextractor.toc;

/**
 * Entry of a Table of Contents
 * @author Sergio
 *
 */
public class TocEntry {
	/**
	 * Title of the entry
	 */
	private String title;
	
	/**
	 * Url to the section
	 */
	private String url;
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getUrl() {
		return url;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		
		return String.format("%s [%s]", title, url);
	}	
}
