package com.myapps.bookextractor.toc;

import java.util.List;

/**
 * Interface for extractors of Table of Contents
 * @author Sergio
 *
 */
public interface TocExtractor {
	
	/**
	 * Given the URL of a book, extracts its Table of Contents.
	 * @param bookUrl Book URL
	 * @return List of ToC entries
	 */
	List<TocEntry> extractTableOfContentsFromUrl(String bookUrl);

}
