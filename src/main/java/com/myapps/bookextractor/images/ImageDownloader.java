package com.myapps.bookextractor.images;

import org.htmlparser.Node;

/**
 * Interface for image downloader
 * @author Sergio
 *
 */
public interface ImageDownloader {

	/**
	 * Downloads the images specified in a given HTML node and saves them into the destination folder.
	 * @param baseUrl Base URL of images
	 * @param node HTML node with the images.
	 * @param destinationFolder Destination folder of the images.
	 */
	void downloadImagesFromHtml(String baseUrl, Node node, String destinationFolder);
	
}
