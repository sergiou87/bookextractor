package com.myapps.bookextractor.images;

import org.htmlparser.Node;
import org.htmlparser.filters.TagNameFilter;
import org.htmlparser.tags.ImageTag;
import org.htmlparser.util.NodeList;

import com.myapps.bookextractor.downloader.HttpDownloader;

/**
 * Abstract template for image downloaders.
 * @author Sergio
 *
 */
public abstract class AbstractImageDownloader implements ImageDownloader {

	/**
	 * Constructors
	 * @param baseUrl Base URL of the book service.
	 */
	public AbstractImageDownloader() {
		super();
	}

	public void downloadImagesFromHtml(String baseUrl, Node node, String destinationFolder)
	{
		NodeList images = new NodeList();
		
		//Get a list of img entities in the HTML node
		node.collectInto(images, new TagNameFilter("img"));
		
		int imagesSize = images.size();
		
		//If eligible, download the image and change the url in the node
		for(int i = 0; i < imagesSize; i++)
		{
			ImageTag imageTag = (ImageTag) images.elementAt(i);
			
			String imageUrl = getImageUrl(baseUrl, imageTag.getImageURL());
			
			if(!isImageEligibleForDownload(imageUrl)) continue;
			
			String imageName = downloadImage(imageUrl, destinationFolder);
			
			imageTag.setImageURL(imageName);
		}
		
	}

	/**
	 * Actual download of the image
	 * @param imageUrl Url of the image
	 * @param destinationFolder Destination folder
	 * @return The name the image created for download
	 */
	private String downloadImage(String imageUrl, String destinationFolder) {
		String imageName = getImageName(imageUrl);
		
		HttpDownloader httpDownloader = createHttpDownloader();
		
		httpDownloader.downloadFromUrlToFile(imageUrl, destinationFolder + imageName);
		
		return imageName;		
	}

	/**
	 * Creates a suitable HTTP downloader
	 * @return
	 */
	protected abstract HttpDownloader createHttpDownloader();

	/**
	 * Creates a name for an image of a given url
	 * @param imageUrl Image url
	 * @return Name of the image.
	 */
	protected abstract String getImageName(String imageUrl);
	
	/**
	 * Decides whether of not the image is eligible for download
	 * @param imageUrl Image url
	 * @return true if the image should be downloaded, false if not
	 */
	protected abstract boolean isImageEligibleForDownload(String imageUrl);

	/**
	 * Builds an image Url using a base Url and the "src" attribute of the image element.
	 * @param baseUrl Base Url
	 * @param imageSrc Image "src" attribute (relative Url)
	 * @return Absolute Image Url
	 */
	protected abstract String getImageUrl(String baseUrl, String imageSrc);
}
