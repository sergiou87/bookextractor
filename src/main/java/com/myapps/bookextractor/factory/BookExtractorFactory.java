package com.myapps.bookextractor.factory;

import com.myapps.bookextractor.BookExtractor;
import com.myapps.bookextractor.utils.ClassFinder;

/**
 * Book extractor factory.
 * @author Sergio
 *
 */
public abstract class BookExtractorFactory {
	
	/**
	 * Given an URL, creates a factory instance for books of that service.
	 * @param url Book URL
	 * @return Factory for books of that service
	 */
	public static BookExtractorFactory createBookExtractorFactoryForUrl(String url)
	{
		BookExtractorFactory bookExtractorFactory = null;

		Class<?>[] classes = ClassFinder.getClasses("");
		
		//Check for all available BookExtractorFactories to see if any can process the given url
		for(Class<?> c : classes)
		{
			if(c != BookExtractorFactory.class && BookExtractorFactory.class.isAssignableFrom(c))
			{
				try {
					BookExtractorFactory tempFactory = (BookExtractorFactory) c.newInstance();
					
					if(tempFactory.isValidUrl(url))
					{
						bookExtractorFactory = tempFactory;
						
						break;
					}
				} catch (InstantiationException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}
		
		return bookExtractorFactory;
	}
	
	/**
	 * Create a book extractor.
	 * @return Book extractor.
	 */
	public abstract BookExtractor createBookExtractor();
	
	/**
	 * Checks whether an URL can be processed by book extractors created by this factory or not.
	 * @param url Book url
	 * @return True if the URL is understood by the book extractor of this factory, otherwise false
	 */
	public abstract boolean isValidUrl(String url);
	
	/**
	 * Does this book extractor need cookies to work properly?
	 * @return True if it needs cookies, false if not
	 */
	public abstract boolean needsCookies();
	
	/**
	 * Just in case this service requires cookies... are they valid right now? It should check the "cookies.txt" file.
	 * @param url Book url to do the test
	 * @return True if the cookies are valid, false if not.
	 */
	public abstract boolean areCookiesValid(String url);
	
	/**
	 * Converts a given book Url to the actual book url. For example: http://www.mydomain.com/mybook/section1.html
	 * is converted to http://www.mydomain.com/mybook
	 * @param url Book url
	 * @return Actual book url
	 */
	public abstract String convertToActualBookUrl(String url);
}
