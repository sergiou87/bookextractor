package com.myapps.bookextractor;

import static org.junit.Assert.*;

import org.junit.Test;

import com.myapps.bookextractor.appl.ApplBookExtractorFactory;
import com.myapps.bookextractor.factory.BookExtractorFactory;


/**
 * Unit test for simple App.
 */
public class ApplTest
{

    /**
     * Test for Appl conversion of book URLs
     */
	@Test
    public void testApplConvertToActualBookUrl()
    {
        BookExtractorFactory bef = new ApplBookExtractorFactory();
        final String correctUrl = "http://www.mydomain.com/library/some/documentation/Something/Another/Theme";
        String url = "";
        
        url = bef.convertToActualBookUrl("http://www.mydomain.com/library/some/documentation");
        assertNull(url);
        
        url = bef.convertToActualBookUrl("http://www.mydomain.com/library/some/documentation/Something/Another/Theme");
        assertEquals(correctUrl, url);
        
        url = bef.convertToActualBookUrl("http://www.mydomain.com/library/some/documentation/Something/Another/Theme/Item/Item.html");
        assertEquals(correctUrl, url);
        
        url = bef.convertToActualBookUrl("http://www.mydomain.com/library/some/#documentation/Something/Another/Theme/Item/Item.html");
        assertEquals(correctUrl, url);
        
        url = bef.convertToActualBookUrl("http://www.mydomain.com/library/some/#documentation/Something/Another/Theme/Item/Item.html#someReference");
        assertEquals(correctUrl, url);
        
        url = bef.convertToActualBookUrl("http://www.mydomain.com/library/some/#documentation/Something/Another/Theme/Item/Item.html#someReference?myquery=things");
        assertEquals(correctUrl, url);
        
        url = bef.convertToActualBookUrl("http://www.mydomain.com/library/some/documentation/Something/Another/Theme/Item/Item.html#someReference?myquery=things");
        assertEquals(correctUrl, url);

        url = bef.convertToActualBookUrl("http://www.mydomain.com/library/some/#documentation/Something/Another/Theme/Item/Item.html#someReference?myquery=things");
        assertEquals(correctUrl, url);
        
    }
}
